package cguim.eclab.hict7minsexercise;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class HICT7minsExercise extends ActionBarActivity {

    int[][] PhotoGroup = {
            {R.drawable.a1,R.drawable.a1_1},//1
            {R.drawable.a2},//2
            {R.drawable.a3,R.drawable.a3_1},//3
            {R.drawable.a4,R.drawable.a4_1},//4
            {R.drawable.a5,R.drawable.a5_1},//5
            {R.drawable.a6,R.drawable.a6_1},//6
            {R.drawable.a7,R.drawable.a7_1},//7
            {R.drawable.a8},//8
            {R.drawable.a9,R.drawable.a9_1},//9
            {R.drawable.a10,R.drawable.a10_1},//10
            {R.drawable.a11,R.drawable.a11_1},//11
            {R.drawable.a12}//12
    };
    int[] actionID= {
            R.string.action01,R.string.action02,R.string.action03,R.string.action04,
            R.string.action05,R.string.action06,R.string.action07,R.string.action08,
            R.string.action09,R.string.action10,R.string.action11,R.string.action12
    };
    int preparetext = R.string.prepare ,resttext = R.string.rest;
    int restimg = R.drawable.a1 /*prepareimg = R.drawable.prepare*/;
    private ImageView imgPhoto;
    private TextView timer, picText, actiontext, preparetv;
    int p = -1; // 圖片的索引(第幾張圖片)
    int group = -1, index = 0;
    //  組別        , 圖碼
    private boolean startflag = false, firstStart = true;
    int tsec = 0;
    int type = 1;// type1=prepare,2=action,3=rest

    int actiontime , preparetime = 1, resttime ;
    public static final String TAG = "MainActivity";
    private SQLiteDatabase db;
    SharedPreferences sp;

    protected Button startbtn;
    protected Button recordviewbtn;
    protected Button pausebtn;
    protected ImageButton preact;
    protected ImageButton nxtact;

    private int actflag;//0表示開始(狀態)，1表示暫停(狀態)

    MediaPlayer player ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hict7mins_exercise);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Timer timer01 = new Timer();

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        timer = (TextView) findViewById(R.id.infotextView2);
        actiontext = (TextView) findViewById(R.id.actiontextView);
        preparetv = (TextView) findViewById(R.id.preparetextView);
        imgPhoto = (ImageView) findViewById(R.id.imageView);
        startbtn = (Button) findViewById(R.id.strbtn);
        preact = (ImageButton)findViewById(R.id.preactbtn);
        nxtact = (ImageButton)findViewById(R.id.nxtactbtn);

        startbtn.setOnClickListener(listener);
        preact.setOnClickListener(listener);
        nxtact.setOnClickListener(listener);

        timer01.schedule(timer_action, 0, 1000);

        DBHelper helper = new DBHelper(HICT7minsExercise.this);
        db = helper.getWritableDatabase();

        //phone call
        TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(new MyPhoneState(), PhoneStateListener.LISTEN_CALL_STATE);

        actflag = 1;
    }

    private OnClickListener listener = new OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.strbtn:
                    if(actflag==0){

                        pause();

                    }else if(actflag==1){
                        start();
                    }
                    break;
                case R.id.preactbtn:
                    pre();
                    break;
                case R.id.nxtactbtn:
                    nxt();
                    break;

            }
        }
    };




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hict7mins_exercise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            pause();
            Intent tosetting = new Intent();
            tosetting.setClass(HICT7minsExercise.this, SettingActivity.class);
            startActivity(tosetting);
            return true;
        }else if(id == R.id.information){
            pause();
            Intent toinfomation = new Intent();
            toinfomation.setClass(this, InformationActivity.class);
            startActivity(toinfomation);
            return true;
        }else if(id == R.id.teachmov){
            pause();
            Intent toteach = new Intent();
            toteach.setClass(this, teachActivity.class);
            startActivity(toteach);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private TimerTask timer_action = new TimerTask() {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            if (startflag) {
                // 如果startflag是true則每秒tsec+1
                tsec++;
                Message message = new Message();
                // 傳送訊息1
                message.what = 1;
                handler.sendMessage(message);
            }
        }

    };
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String actiontime_string = sp.getString(getString(R.string.exercise_time_key), "30" );
            actiontime = Integer.valueOf(actiontime_string);

            String resttime_string = sp.getString(getString(R.string.rest_time_key), "7");
            resttime = Integer.valueOf(resttime_string);

            String s = "";
            s = s + tsec;
            timer.setText(s);

            switch (type) {
                case 1:// prepare
                    if (tsec > preparetime) {
                        preparetv.setVisibility(View.INVISIBLE);
                        type = 2;
                        reset();
                    }
                    break;
                case 2:
                    if(tsec > actiontime && group < PhotoGroup.length -1){
                        preact.setVisibility(View.INVISIBLE);
                        nxtact.setVisibility(View.INVISIBLE);
                        imgPhoto.setImageResource(restimg);
                        actiontext.setText(resttext);
                        tsec = 0;
                        timer.setText("0");
                        type = 3;
                    }else if (tsec <= actiontime) {
                        preact.setVisibility(View.VISIBLE);
                        nxtact.setVisibility(View.VISIBLE);
                        imgPhoto.setImageResource(PhotoGroup[group][index]);
                        actiontext.setText(actionID[group]);
                        index++;
                        if (index >= PhotoGroup[group].length) {
                            index = 0;
                        }


                    }else if (tsec > actiontime && group == PhotoGroup.length -1) {
                        type = 1;
                        stop();
                        preact.setVisibility(View.INVISIBLE);
                        nxtact.setVisibility(View.INVISIBLE);

                        actiontext.setText("恭喜結束");
                        addRecord();
                    }
                    break;
                case 3:// rest
                    if (tsec > resttime) {
                        player = MediaPlayer.create(HICT7minsExercise.this,R.raw.changesound);
                        player.start();
                        preparetv.setVisibility(View.VISIBLE);
                        preparetv.setText("↓下個動作↓");
                        changeGroup(1);
                        tsec = 0;
                        timer.setText("0");
                        type = 1;
                    }
                    break;
            }

        }
    };




    private void changeGroup(int act){
        index = 0;
        group = group + act;
        if(group == PhotoGroup.length){
            group = 0;
        }
        else if(group < 0){
            return;
            //group = PhotoGroup.length -1;
        }
        imgPhoto.setImageResource(PhotoGroup[group][index]);
        actiontext.setText(actionID[group]);
        reset();
    }

    private void start() {
        Log.d(TAG, "start()");

        // 開始執行
        startflag = true;
        actflag = 0;
        startbtn.setText("暫停");

        if (firstStart) {
            changeGroup(1);
            firstStart = false;
        }

    }

    private void pause() {
        Log.d(TAG, "pause()");

        startflag = false;
        actflag=1;
        startbtn.setText("開始");

    }

    private void pre() {
        Log.d(TAG, "pre()");

        if(group<=0){

        }else{
            changeGroup(-1);
            pause();
        }
    }

    private void nxt() {
        Log.d(TAG, "nxt()");

        if(group>=PhotoGroup.length-1){

        }else{
            changeGroup(1);
            pause();
        }

    }

    private void stop() {
        Log.d(TAG, "stop()");
        changeGroup(1);
        // 回圖1
//		imgPhoto.setImageResource(PhotoGroup[0][0]);
        // 歸零
        tsec = 0;
        timer.setText("0");
        startflag = false;
        actflag=1;
        startbtn.setText("開始");

    }


    private void reset() {
        tsec = 0;
        timer.setText("0");
        startflag = true;
    }



    private void addRecord() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        ContentValues newRow = new ContentValues();
        newRow.put(DBHelper.DATE, dateFormat.format(date));
        newRow.put(DBHelper.FINISH, true);
        newRow.put(DBHelper.MEMO, p);
        db.insert(DBHelper.TABLE_NAME, null, newRow);
    }
    class MyPhoneState extends PhoneStateListener {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);

            switch (state) {
                case 0x00000001:
                    pause();
                    break;

                default:
                    break;
            }
        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {// 捕捉返回鍵
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            ConfirmExit();// 按返回鍵，則執行退出確認
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void ConfirmExit() {// 退出確認
        AlertDialog.Builder ad = new AlertDialog.Builder(HICT7minsExercise.this);
        ad.setTitle("離開");
        ad.setMessage("確定要離開?");
        ad.setPositiveButton("是", new DialogInterface.OnClickListener() {// 退出按鈕
            public void onClick(DialogInterface dialog, int i) {
                HICT7minsExercise.this.finish();// 關閉activity

            }
        });
        ad.setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                // 不退出不用執行任何操作
            }
        });
        ad.show();// 示對話框
    }
}
