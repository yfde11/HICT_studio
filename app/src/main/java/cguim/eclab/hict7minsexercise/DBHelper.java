package cguim.eclab.hict7minsexercise;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;// 資料庫版本
    public static final String DB_NAME = "finely7minapp";
    public static final String TABLE_NAME = "record";
    public static final String ID = "_id";
    public static final String DATE = "date";
    public static final String FINISH = "finish";
    public static final String MEMO = "memo";

    public DBHelper(Context context, String name, CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }
    public DBHelper(Context context) {
        this(context, DB_NAME, null, VERSION);
    }
    public DBHelper(Context context, String name, int version) {
        this(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String DATABASE_CREATE_TABLE = "create table " + TABLE_NAME + "("
                + ID +" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,"
                + DATE + " VARCHAR,"
                + FINISH + " BOOLEAN,"
                + MEMO + " VARCHAR" + ")";
        db.execSQL(DATABASE_CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // oldVersion=舊的資料庫版本；newVersion=新的資料庫版本
        db.execSQL("DROP TABLE IF EXISTS record"); // 刪除舊有的資料表
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        // TODO 每次成功打開數據庫後首先被執行
    }

    @Override
    public synchronized void close() {
        super.close();
    }

}

